# frozen_string_literal: true

require_relative "style/version"

module Kettle
  module Soup
    module Style
      class Error < StandardError; end
      # Your code goes here...
    end
  end
end
